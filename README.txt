The Nodemoter module is a simple module that demotes nodes after a set time
period has elapsed since the last time that node was changed. In other words,
it "unchecks" the "promote to front page" option. It does this when the date
configured by the user is greater than the node's last changed timestamp.

Behind the scenes this uses PHP's strtotime() function, so anything that can
be used there will work for a cutoff time. The default is "-1 month". This
means that, for nodes promoted to the front page, if those nodes' last changed
timestamp is more than one month behind the current time, it will be "demoted"
and removed from the front page.

The module is configurable via the Modules list and the content section in the
Configuration page. There is only one configuration parameter: the cut-off
time (e.g. "-1 month").
